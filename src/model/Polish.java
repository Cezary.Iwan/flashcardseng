package model;

public class Polish {
    private int id;
    private String word;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
    public Polish() {}
    public Polish(int id, String word) {
        this.id = id;
        this.word = word;
    }

    @Override
    public String toString() {
        return "["+id+"] - "+word;
    }

}
