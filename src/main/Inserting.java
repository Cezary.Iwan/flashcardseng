package main;

import model.English;
import model.Polish;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class Inserting {

    DBConnection db = new DBConnection();

//    public void inserting() {
//
//        db.insertEnglish("test");
//        db.insertPolish("test");
//
//        List<English> englishWords = db.selectEnglish();
//        List<Polish> polishWords = db.selectPolish();
//        System.out.println("Slowka ang: ");
//        for (English e : englishWords)
//            System.out.println(e);
//        for (Polish p : polishWords)
//            System.out.println(p);
//
//        db.closeConnection();
//
//
//    }

    File polFile = new File("C:\\Users\\Administrator\\Documents\\FlashcardsENG\\src\\polish");
    File engFile = new File("C:\\Users\\Administrator\\Documents\\FlashcardsENG\\src\\english");


    public void convertToArg() throws FileNotFoundException {
        Scanner scanner = new Scanner(polFile);
        for (int i = 0; i < 123; i++) {
            {
                String line = scanner.nextLine();
                db.insertPolish(line, i);
            }
        }
        scanner = new Scanner(engFile);
        for (int i = 0; i < 123; i++) {


            String line = scanner.nextLine();
            db.insertEnglish(line, i);
        }

    }

    public void listWords() {
        List<English> englishWords = db.selectEnglish();
        List<Polish> polishWords = db.selectPolish();
        System.out.println("Slowka ang: ");
        for (English e : englishWords)
            System.out.println(e);
        System.out.println("Slowka pol: ");
        for (Polish p : polishWords)
            System.out.println(p);

        db.closeConnection();
    }
}