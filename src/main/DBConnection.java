package main;

import model.English;
import model.Polish;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;


public class DBConnection {

    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:dbconnection.db";

    private Connection conn;
    private Statement stat;

    public DBConnection() {
        try {
            Class.forName(DBConnection.DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
            e.printStackTrace();
        }

        try {
            conn = DriverManager.getConnection(DB_URL);
            stat = conn.createStatement();
        } catch (SQLException e) {
            System.err.println("Problem z otwarciem polaczenia");
            e.printStackTrace();
        }

        createTables();
    }

    public boolean createTables() {
        String createEnglish = "CREATE TABLE IF NOT EXISTS english (id_english INTEGER PRIMARY KEY AUTOINCREMENT, word varchar(255))";
        String createPolish = "CREATE TABLE IF NOT EXISTS polish (id_polish INTEGER PRIMARY KEY AUTOINCREMENT, word varchar(255))";

        try {
            stat.execute(createEnglish);
            stat.execute(createPolish);
        } catch (SQLException e) {
            System.err.println("Blad przy tworzeniu tabeli");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean insertEnglish(String word, int id) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into english values (?, ?);");
            prepStmt.setInt(1, id);
            prepStmt.setString(2, word);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean insertPolish(String word, int id) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into polish values (?, ?);");
            prepStmt.setInt(1, id);
            prepStmt.setString(2, word);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad");
            return false;
        }
        return true;
    }
    public ResultSet select(String query) {
        try {
            return stat.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void delete(String deleteQuery) {

        try (PreparedStatement pstmt = conn.prepareStatement(deleteQuery)) {

            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }





    public List<English> selectEnglish() {
        List<English> english = new LinkedList<>();
        try {
            ResultSet result = select("SELECT * FROM english");
            int id;
            String word;
            while (result.next()) {
                id = result.getInt("id_english");
                word = result.getString("word");
                english.add(new English(id, word));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return english;
    }

    public English selectGivenEnglish(int id) {
        English ret = new English();
        try {
            ResultSet res = stat.executeQuery("SELECT * FROM english WHERE id_english=" + id);
            int nr;
            String word;
            while (res.next()) {
                id = res.getInt("id_english");
                word = res.getString("word");
                ret = new English(id, word);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;

        }
        return ret;
    }

    public Polish selectGivenPolish(int id) {
        Polish ret = new Polish();
        try {
            ResultSet res = stat.executeQuery("SELECT * FROM polish WHERE id_polish=" + id);
            int nr;
            String word;
            while (res.next()) {
                id = res.getInt("id_polish");
                word = res.getString("word");
                ret = new Polish(id, word);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;

        }
        return ret;
    }

    public List<Polish> selectPolish() {
        List<Polish> polish = new LinkedList<>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM polish");
            int id;
            String word;
            while (result.next()) {
                id = result.getInt("id_polish");
                word = result.getString("word");
                polish.add(new Polish(id, word));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return polish;
    }

    public void closeConnection() {
        try {
            conn.close();
        } catch (SQLException e) {
            System.err.println("Problem z zamknieciem polaczenia");
            e.printStackTrace();
        }
    }
}