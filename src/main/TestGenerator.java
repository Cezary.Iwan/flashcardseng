package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class TestGenerator {

    DBConnection db = new DBConnection();
//    String[] polishArg = new String[123];
//    String[] englishArg = new String[123];
//    File polFile = new File("C:\\Users\\Administrator\\Documents\\FlashcardsENG\\src\\polish");
//    File engFile = new File("C:\\Users\\Administrator\\Documents\\FlashcardsENG\\src\\english");

//    public TestGenerator() throws FileNotFoundException {
//        convertToArg();
//    }

//    public void convertToArg() throws FileNotFoundException {
//        Scanner scanner = new Scanner(polFile);
//        for (int i = 0; i < 123; i++) {
//            {
//                String line = scanner.nextLine();
//                polishArg[i] = line;
//            }
//        }
//        scanner = new Scanner(engFile);
//        for (int i = 0; i < 123; i++) {
//            String line = scanner.nextLine();
//            englishArg[i] = line;
//        }
//    }


    public void generateTest(int nr, boolean order) throws FileNotFoundException {
        int whichLine;
        for (int i = 0; i < nr; i++) {
            if (!order) {
                Random random = new Random();
                whichLine = random.nextInt(nr);
            } else {
                whichLine = i;
            }
            generateQuestion(whichLine, db.selectGivenPolish(whichLine).getWord(), db.selectGivenEnglish(whichLine).getWord());

        }


    }

    private void generateQuestion(int i, String pl, String eng) throws FileNotFoundException {

        System.out.println("Wprowadź odpowiednik podanej frazy w języku angielskim:" +
                "\n" + (i + 1) + "." + pl);
        Scanner input = new Scanner(System.in);
        String answer = input.nextLine();

        if (answer.equals("3") || answer.equals("x")) {
            run();
        } else if (answer.equals(eng)) {

            System.out.println("Brawo!");
        } else {
            System.out.println("Źle, poprawna odpowiedź to: " + eng + "\n");
        }
    }

    public static void welcomingText() {
        System.out.println("Witaj! Wybierz tryb nauki, poprzez wpisanie cyfry dolaczonej do wypisanej opcji i zatwierdzenie enterem:" +
                "\n1.Baza slowek" +
                "\n2.Test" +
                "\n3.Zakończ program (ncisnij 3, badz x)");
    }

    public static void run() throws FileNotFoundException {
        Base base = new Base();
        TestGenerator testGenerator = new TestGenerator();

        welcomingText();
        Scanner scan = new Scanner(System.in);
        String answer = scan.nextLine();
        while (!answer.equals("3") && !answer.equals("x")) {


            if (answer.equals("1")) {
                base.returnBase();
            } else if (answer.equals("2")) {
                System.out.println("1. Wpisz zakres numer slowa, do ktorego chcesz ustalic zakres." +
                        "\n2. Zatwierdz enterem." +
                        "\n3. Wpisz true, jesli chcesz, aby slowa byly wyswietlane po kolei, badz false, jesli chcesz, aby slowa byly wyswietlane losowo." +
                        "\n4. Zatwierdz enterem." +
                        "\n W kazdej chwili mozesz powrocic do wyboru trybu nauki poprzez nacisniecie x, badz 3.");
                String givenRange = scan.nextLine();
                int givenRangeInt = Integer.parseInt(givenRange);
                String givenOrder = scan.nextLine();
                boolean givenOrderBoolean = Boolean.parseBoolean(givenOrder);
                testGenerator.generateTest(givenRangeInt, givenOrderBoolean);
            } else {
                System.out.println("Wybierz opcję 1-3!");

            }
            welcomingText();
            answer = scan.nextLine();
        }
    }
}
