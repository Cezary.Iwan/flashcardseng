Aplikacja służy do nauki języka angielskiego, poprzez fiszki. Jej aktualne, główne funkcjonalości to:
1) Wyświetlenie bazy słówek.
2) Test, z opcją wyboru ilości słówek i wyboru, czy słówka mają być wyświetlane losowo, bądź po kolei.
Aplikacja posiada bazę danych SQLite. Obecnie moje główne cele dla tego projektu, to refactoring kodu, przejście w 100% na bazy danych z plików tekstowych(baza słówek
jest nadal zapisana w pliku tekstowym), dodanie Springa oraz nowych funkcjonalności, m.in. opcji dodawania słówek do bazy przez użytkownika.